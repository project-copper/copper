import os
import sys
import subprocess
from rich import print, box
from rich.console import Console
from rich.table import Table

from flask import Flask, redirect, url_for, request
import json
import torch
import requests
import subprocess
from transformers import AutoConfig, GPTNeoForCausalLM, AutoTokenizer
import tarfile
import os
import re
import time
from threading import Timer

import model.start

console = Console()

# We have to detect the os first in order to run the commands or forcing the user to install requirements
table = Table(show_header=False, box=box.ROUNDED)
table.add_row(":wrench: Installing dependencies...")
console.print(table)

if sys.platform.startswith('win32'):
    console.print(
        "You have to get [bold black on blue]aria2[/bold black on blue] and [bold black on blue]7zip[/bold black on blue] console utility.")
elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
    subprocess.run(["sudo", "apt", "install", "aria2", "p7zip-full"])
    console.clear()

console.print("Done installing dependencies", style="black on green")

# model to use
model_name = "GPT-2.7B-horni"

model.start