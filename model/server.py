app = Flask(__name__)

if connect_method == "Cloudflare":
    from flask_cloudflared import run_with_cloudflared

    run_with_cloudflared(app)
elif connect_method == "Ngrok":
    from flask_ngrok import run_with_ngrok

    run_with_ngrok(app)


@app.route("/")
def home():
    return "<h1>KoboldAI Colab Service Running!</h1>"


@app.route('/request', methods=['POST'])
def koboldrequest():
    if request.method == 'POST':
        try:
            clear_output()
            js = request.json
            txt = js["text"]
            min = js["min"]
            max = js["max"]
            rep_pen = js["rep_pen"]
            temp = js["temperature"]
            top_p = js["top_p"]

            # Compatability with un-updated clients
            if ("numseqs" in js):
                numseqs = js["numseqs"]
            else:
                numseqs = 1

            if ("retfultxt" in js):
                retfultxt = js["retfultxt"]
            else:
                retfultxt = True

            print(colored("Received Data: {0}".format(txt), "yellow"))

            torch.cuda.empty_cache()
            print(colored("Generating text, please wait...", "green"))

            tokens = tokenizer(txt, return_tensors="pt").input_ids.to("cpu")
            ids = tokens.cuda()

            gen_tokens = model.generate(
                ids.long().cuda(),
                do_sample=True,
                min_length=min,
                max_length=max,
                temperature=temp,
                top_p=top_p,
                repetition_penalty=rep_pen,
                use_cache=True,
                bad_words_ids=bad_words_ids,
                num_return_sequences=numseqs
            ).long()

            genout = []
            for tkns in gen_tokens:
                if (not retfultxt):
                    # Strip context tokens out of returned sequences
                    dif = (len(tkns) - len(tokens[0])) * -1
                    tkns = tkns[dif:]
                tkns = list(filter(lambda a: a != 50256, tkns))
                genout.append(tokenizer.decode(tkns))
            torch.cuda.empty_cache()

            if (len(genout) > 0 and genout[0] != ""):
                if (retfultxt):
                    # Outdated client, send old JSON format
                    print(colored("Generated Text: {0}".format(genout[0]), "cyan"))
                    response = app.response_class(
                        response=json.dumps({"data": {"text": genout[0]}}),
                        status=200,
                        mimetype='application/json'
                    )
                else:
                    # New client format with numseq support
                    i = 0
                    for seq in genout:
                        print(colored("[Result {0}]\n{1}".format(i, seq), "cyan"))
                        i += 1
                    response = app.response_class(
                        response=json.dumps({"data": {"seqs": genout}}),
                        status=200,
                        mimetype='application/json'
                    )

                return response
            else:
                print(colored("[ERROR] Something went wrong during generation!", "red"))
                response = app.response_class(
                    response=json.dumps({"error": {"extensions": {"code": "Something went wrong during generation!"}}}),
                    status=400,
                    mimetype='application/json'
                )

            js = {}
            tokens = []
            ids = []
            gen_tokens = []
            genout = ""
            response = {}

        except Exception as e:
            print(colored("[ERROR] Something went wrong during generation!", "red"))
            print(colored("{0}".format(e), "red"))
            response = app.response_class(
                response=json.dumps(
                    {"error": {"extensions": {"code": "Something went wrong during generation! {0}".format(e)}}}),
                status=400,
                mimetype='application/json'
            )


print(colored("Starup complete! Running web service.", "green"))
app.run()