# TODO: Cleanup
import os
import subprocess
from rich.progress import track
from rich.progress import BarColumn, Progress, TimeRemainingColumn



def main():

    base_path = "./Kdata"

    # TODO: replace with rich progress bar
    # def progress(value, max=100):
    #     return HTML("""
    #         <progress
    #             value='{value}'
    #             max='{max}',
    #             style='width: 100%'
    #         >
    #             {value}
    #         </progress>
    #     """.format(value=value, max=max))
    progress = Progress(
        '[progress.description]{task.description}',
        BarColumn(),
        '[magenta]{task.completed} of {task.total}',
        TimeRemainingColumn()
    )

    def download_file(model_name, model_url, filename):
        process = subprocess.Popen(
            ["aria2c", f"--out={base_path}drive/MyDrive/KoboldAI/models/" + str(filename), "--split=18", "--summary-interval=3",
             "--max-connection-per-server=2", "https://ipfs.fleek.co/ipfs/" + str(model_url),
             "https://birds-are-nice.me/ipfs/" + str(model_url), "https://ipfs.foxgirl.dev/ipfs/" + str(model_url),
             "https://ipfs.drink.cafe/ipfs/" + str(model_url), "https://ipfs.decoo.io/ipfs/" + str(model_url),
             "https://infura-ipfs.io/ipfs/" + str(model_url), "https://ipfs.greyh.at/ipfs/" + str(model_url),
             "https://10.via0.com/ipfs/" + str(model_url), "https://cf-ipfs.com/ipfs/" + str(model_url),
             "https://gateway.originprotocol.com/ipfs/" + str(model_url),
             "https://gateway.pinata.cloud/ipfs/" + str(model_url), "https://ipfs.yt/ipfs/" + str(model_url),
             "https://dweb.link/ipfs/" + str(model_url), "https://ninetailed.ninja/ipfs/" + str(model_url),
             "https://ipfs.mrh.io/ipfs/" + str(model_url), "https://ipfs.astyanax.io/ipfs/" + str(model_url)],
            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        # TODO: rich progress
        # print(colored("Downloading " + model_name + " model to your GDrive", "magenta"))
        # out = display(progress(0, 100), display_id=True)
        # percentage = 0
        # for line in iter(process.stdout.readline, b''):
        #     if process.poll() is not None:
        #         break
        #     try:
        #         percentage = (re.search("(?P<percentage>[0-9]{1,3})%", str(line)).group("percentage"))
        #         out.update(progress(percentage, 100))
        #     except:
        #         pass
        # out.update(progress(100, 100))
        # print(colored("* Finished downloading the model to your GDrive.", "magenta"))
        with progress:
            task = progress.add_task("Downloading " + model_name + " model to your GDrive", total=len(files))

            for file in files:
                download_file(file)
                progress.update(task, advance=1)

    def unpack_7z(path):
        process = subprocess.Popen(["7z", "x", "-bsp1", path], stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                   universal_newlines=True)
        print(colored("Extracting " + model_name + " model to Colab", "magenta"))
        out = display(progress(0, 100), display_id=True)
        percentage = 0
        while True:
            output = process.stdout.read(24)
            if percentage == 100:
                break
            try:
                percentage = (re.search("(?P<percentage>[0-9]{1,3})%", str(output)).group("percentage"))
                out.update(progress(percentage, 100))
            except:
                try:
                    done = re.search("Eve", str(output))
                    if done:
                        out.update(progress(100, 100))
                        print(colored("* Finished extraction of " + model_name + " to Colab", "magenta"))
                        break
                    else:
                        pass
                except:
                    pass
                time.sleep(.1)
                pass
        out.update(progress(100, 100))
        print(colored("* Finished extracting the model to Colab.", "magenta"))

    #print(colored("Requesting Google Drive access...", "magenta"))
    #drive.mount('/content/drive/')

    def getModel(model_name):
        if model_name == "GPT-J-6B":
            if os.path.exists(base_path+"/content/j6b_ckpt"):
                print(f"* Found model")
            else:
                model_url = "QmRfJXBxPXLozPZWXDaKdqBBeVj37mtQMMFJg3kysjvpvM?filename=j6b_ckpt.7z"
                if os.path.isfile(base_path+"/content/drive/MyDrive/KoboldAI/models/j6b_ckpt.7z"):
                    print(f"* Found model in KoboldAI folder!")
                else:
                    download_file(model_name, model_url, "j6b_ckpt.7z")
                unpack_7z(base_path+"/content/drive/MyDrive/KoboldAI/models/j6b_ckpt.7z")
        elif model_name == "2.7B-horni":
            if os.path.exists(base_path+"/content/gpt-neo-2.7B-horni"):
                print(f"* Found model on Colab instance!")
            else:
                model_url = "QmcJyDmvVX5kY7ppoQDrMQELWxY2WSEdt54eKtJYkjFEYG?filename=gpt-neo-2.7B-horni.7z"
                if os.path.isfile(base_path+"/content/drive/MyDrive/KoboldAI/models/gpt-neo-2.7B-horni.7z"):
                    print(f"* Found model in KoboldAI folder!")
                else:
                    download_file(model_name, model_url, "gpt-neo-2.7B-horni.7z")
                unpack_7z(base_path+"/content/drive/MyDrive/KoboldAI/models/gpt-neo-2.7B-horni.7z")

    def initModel(model_name):
        model = None
        tokenizer = None

        print(colored("Initializing model, please wait...", "magenta"))

        if model_name == "2.7B-horni":
            model_name = model_name
            # Thanks to finetune for some of this startup code, I'm really not
            # familiar with the Colab environment
            model = None
            tokenizer = None

            checkpoint = torch.load("gpt-neo-" + model_name + "/pytorch_model.bin", map_location="cuda:0")
            model = GPTNeoForCausalLM.from_pretrained("gpt-neo-" + model_name, state_dict=checkpoint).half().to(
                "cuda").eval()
            for k in list(checkpoint.keys()):
                del checkpoint[k]
            del checkpoint

            # Initialize the tokenizer and set up the bad_words_ids to exclude Author's Note tags
            tokenizer = AutoTokenizer.from_pretrained("gpt2")
            vocab = tokenizer.get_vocab()
            vocab_keys = vocab.keys()
            find_keys = lambda char: [key for key in vocab_keys if key.find(char) != -1]
            bad_words = []
            bad_words_ids = []

            bad_words.extend(find_keys("["))
            bad_words.extend(find_keys(" ["))
            for key in bad_words:
                bad_id = vocab[key]
                bad_words_ids.append([bad_id])

        elif model_name == "GPT-J-6B":
            # Initialize the model
            config = AutoConfig.from_pretrained("EleutherAI/gpt-neo-2.7B")
            config.attention_layers = ["global"] * 28
            config.attention_types = [["global"], 28]
            config.num_layers = 28
            config.num_heads = 16
            config.hidden_size = 256 * config.num_heads
            config.vocab_size = 50400
            config.rotary = True
            config.rotary_dim = 64
            config.jax = True

            try:
                from collections.abc import MutableMapping
            except ImportError:
                from collections import MutableMapping

            class Checkpoint(MutableMapping):
                def __init__(self):
                    self.checkpoint = torch.load("j6b_ckpt/m.pt", map_location="cpu")

                def __len__(self):
                    return len(self.checkpoint)

                def __getitem__(self, key):
                    return torch.load(self.checkpoint[key], map_location="cpu")

                def __setitem__(self, key, value):
                    return

                def __delitem__(self, key, value):
                    return

                def keys(self):
                    return self.checkpoint.keys()

                def __iter__(self):
                    for key in self.checkpoint:
                        yield (key, self.__getitem__(key))

                def __copy__(self):
                    return Checkpoint()

                def copy(self):
                    return Checkpoint()

            model = GPTNeoForCausalLM.from_pretrained(pretrained_model_name_or_path=None, config=config,
                                                      state_dict=Checkpoint())

            # Initialize the tokenizer and set up the bad_words_ids to exclude Author's Note tags
            tokenizer = AutoTokenizer.from_pretrained("gpt2")
            vocab = tokenizer.get_vocab()
            vocab_keys = vocab.keys()
            find_keys = lambda char: [key for key in vocab_keys if key.find(char) != -1]
            bad_words = []
            bad_words_ids = []

            bad_words.extend(find_keys("["))
            bad_words.extend(find_keys(" ["))
            bad_words.extend(find_keys("<|endoftext|>"))
            for key in bad_words:
                bad_id = vocab[key]
                bad_words_ids.append([bad_id])

        clear_output()
        print(colored("DONE!", "green"))


if __name__ == '__main__':
    main()
