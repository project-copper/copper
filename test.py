import re
import subprocess

from rich.console import Console
from rich.progress import BarColumn, Progress, TimeRemainingColumn, TransferSpeedColumn

base_path = "./Kdata/"
model_name = "2.7B-horni"
model_url = "QmcJyDmvVX5kY7ppoQDrMQELWxY2WSEdt54eKtJYkjFEYG?filename=gpt-neo-2.7B-horni.7z"

console = Console()

progress = Progress(
    '[progress.description]{task.description}',
    BarColumn(),
    '[magenta][progress.percentage]{task.percentage:>3.1f}% downloaded',
    TransferSpeedColumn(),
    TimeRemainingColumn()
)


def download_file(model_name, model_url, filename):
    process = subprocess.Popen(
        ["aria2c", f"--out={base_path}drive/MyDrive/KoboldAI/models/" + str(filename), "--split=18",
         "--summary-interval=3",
         "--max-connection-per-server=2", "https://ipfs.fleek.co/ipfs/" + str(model_url),
         "https://birds-are-nice.me/ipfs/" + str(model_url), "https://ipfs.foxgirl.dev/ipfs/" + str(model_url),
         "https://ipfs.drink.cafe/ipfs/" + str(model_url), "https://ipfs.decoo.io/ipfs/" + str(model_url),
         "https://infura-ipfs.io/ipfs/" + str(model_url), "https://ipfs.greyh.at/ipfs/" + str(model_url),
         "https://10.via0.com/ipfs/" + str(model_url), "https://cf-ipfs.com/ipfs/" + str(model_url),
         "https://gateway.originprotocol.com/ipfs/" + str(model_url),
         "https://gateway.pinata.cloud/ipfs/" + str(model_url), "https://ipfs.yt/ipfs/" + str(model_url),
         "https://dweb.link/ipfs/" + str(model_url), "https://ninetailed.ninja/ipfs/" + str(model_url),
         "https://ipfs.mrh.io/ipfs/" + str(model_url), "https://ipfs.astyanax.io/ipfs/" + str(model_url)],
        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    # TODO: rich progress
    # print(colored("Downloading " + model_name + " model to your GDrive", "magenta"))
    # out = display(progress(0, 100), display_id=True)
    # percentage = 0
    # for line in iter(process.stdout.readline, b''):
    #     if process.poll() is not None:
    #         break
    #     try:
    #         percentage = (re.search("(?P<percentage>[0-9]{1,3})%", str(line)).group("percentage"))
    #         out.update(progress(percentage, 100))
    #     except:
    #         pass
    # out.update(progress(100, 100))
    # print(colored("* Finished downloading the model to your GDrive.", "magenta"))
    with progress:
        task = progress.add_task("Downloading " + model_name + " model to your GDrive", total=100)
        percentage = 0
        for line in iter(process.stdout.readline, b''):
            if process.poll() is not None:
                break
            try:
                percentage = (re.search("(?P<percentage>[0-9]{1,3})%", str(line)).group("percentage"))
                progress.update(task, advance=1, refresh=True)
            except:
                pass
            print(percentage)
        progress.update(task, completed=1, refresh=True)
        console.print("* Finished downloading the model to your GDrive.", style="magenta")


download_file(model_name, model_url, "gpt-neo-2.7B-horni.7z")
